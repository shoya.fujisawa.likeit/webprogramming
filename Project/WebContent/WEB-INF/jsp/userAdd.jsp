<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>ユーザ登録画面</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/original/userAdd.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js">
    <script src="js/bootstrap.min.js"></script>
</head>
<body>
<header>
    <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">



    <ul class="navbar-nav px-3">
     <li class="navbar-text">${userInfo.name} さん</li>
     <li class="nav-item text-nowrap">
      <a class="nav-link　logout-link" href="LogoutServlet">ログアウト</a>
     </li>
    </ul>
   </nav>
    </header>

  <div class="container">
    <h1>ユーザ新規登録</h1>
    <p><span style="color:#ff0000;">${requestScope.errMsg}</span></p>
    <div class="panel panel-default">
        <div class="panel-body">
          <div class="panel-body">
            <form method="post" action="UserAddServlet" class="form-horizontal">
              <div class="form-group row">
                <label for="user-id" class="control-label col-sm-4">ログインID</label>
                <div class="col-sm-6">
                  <input type="text" name="user-id" id="user-id" class="form-control" value="${requestScope.userId}"/>
                </div>
              </div>
              <div class="form-group row">
                <label for="password" class="control-label col-sm-4">パスワード</label>
                <div class="col-sm-6">
                  <input type="password" name="password" id="password" class="form-control" />
                </div>
              </div>
              <div class="form-group form-margin row">
                <label for="password-confirm" class="control-label col-sm-4">パスワード(確認)</label>
                <div class="col-sm-6">
                  <input type="password" name="password-confirm" id="password-confirm" class="form-control"/>
                </div>
              </div>
              <div class="form-group form-margin row">
                <label for="user-name" class="control-label col-sm-4">ユーザ名</label>
                <div class="col-sm-6">
                  <input type="text" name="user-name" id="user-name" class="form-control"  value="${requestScope.userName}"/>
                </div>
              </div>
              <div class="form-group form-margin row">
                <label for="continent" class="control-label col-sm-4">生年月日</label>

                  <div class="row col-sm-5">
                    <input type="date" name="birth-day" id="birth-day" class="form-control" size="50" value="${requestScope.userBirthdate}"/>
                  </div>
              </div>
              <div class="text-center">
                <button type="submit" value="検索">登録</button>
              </div>

              <div class="col-xs-4">
                <a href="UserListServlet">戻る</a>
              </div>
              </form>
              </div>





          </div>
        </div>
      </div>

</body>
</html>