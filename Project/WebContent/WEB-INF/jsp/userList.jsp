<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>ユーザ一覧画面</title>
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/original/userList.css" rel="stylesheet">
</head>
<body>
  <header>
   <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">


    <ul class="navbar-nav px-3">
     <li class="navbar-text">${userInfo.name} さん</li>
     <li class="nav-item text-nowrap">
      <a class="nav-link logout-link" href="LogoutServlet">ログアウト</a>
     </li>
    </ul>

   </nav>

  </header>
 <div class="container">
  <h1>ユーザ一覧</h1>

  <div class="create-user-button">
    <a href="UserAddServlet">新規登録</a>
  </div>

  <div class="search-form-area">
   <div class="panel-body">
    <form method="post" action="UserListServlet">
      <div class="input-loginid row">
        <label for="loginid" class="col-sm-3 col-form-label">ログインID</label>
        <input type="text" name="loginid" class="form1 form-control">
      </div>

      <div class="input-user-name row">
        <label for="user-name" class="col-sm-3 col-form-label">ユーザ名</label>
        <input type="text" name="user-name" class="form2 form-control">
      </div>


      <div class="input-birth-date row">
       　<label for="birth-date" class="row col-sm-3 col-form-label">生年月日</label>
     　  <div class="row col-sm-9">
              <div class="col-sm-5">
                <input type="date" name="date-start" id="date-start" class="form-control"/>
              </div>

              <div class="col-sm-1 text-center">
                〜
              </div>
              <div class="col-sm-5">
                <input type="date" name="date-end" id="date-end" class="form-control"/>
              </div>
        </div>
        </div>


        <div class="text-right">
            <button type="submit" value="検索">検索</button>
        </div>


    </form>
    </div>
  </div>
  </div>

  <div class=table-responsive>
     <table class="table table-striped">
     <table class="table table-bordered">
       <thead class="thead-light">
        <tr>
          <th>ログインID</th>
          <th>ユーザ名</th>
          <th>生年月日</th>
          <th></th>
        </tr>
       </thead>

      <tbody>



        <c:forEach var="userlist" items="${requestScope.userList}" >
        <tr>


          <td>${userlist.loginid}</td>
          <td>${userlist.name}</td>
          <td>${userlist.birthdate}</td>






          <td>
            <c:if test="${userInfo.name=='管理者'}">
              <a class="btn btn-primary" href="UserDetailServlet?id=${userlist.id}">詳細</a>
              <a class="btn btn-success" href="UserUpdateServlet?id=${userlist.id}">更新</a>
              <a class="btn btn-danger" href ="UserDeleteServlet?id=${userlist.id}">削除</a>
            </c:if>

            <c:if test="${userInfo.name!='管理者'}">
              <a class="btn btn-primary" href="UserDetailServlet?id=${userlist.id}">詳細</a>
            </c:if>

            <c:if test="${userInfo.name==userlist.name}">
              <a class="btn btn-success" href="UserUpdateServlet?id=${userlist.id}">更新</a>
            </c:if>

          </td>
        </tr>
        </c:forEach>

       <%-- <tr>


          <td><c:out value="${user.loginid}"/></td>
          <td><c:out value="${user.name}"/></td>
          <td><c:out value="${user.birthdate}"/></td>

          <td>

              <a class="btn btn-primary" href="UserDetailServlet">詳細</a>
              <a class="btn btn-success" href="UserUpdateServlet">更新</a>
              <a class="btn btn-danger" href ="UserDeleteServlet">削除</a>
          </td>
        </tr> --%>


      </tbody>
      </table>
      </table>

   </div>
</body>
</html>