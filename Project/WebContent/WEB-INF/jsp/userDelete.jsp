<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>ユーザ削除画面</title>
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/original/userDelete.css" rel="stylesheet">
</head>
<body>
  <header>
      <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">



    <ul class="navbar-nav px-3">
     <li class="navbar-text">${userInfo.name} さん</li>
     <li class="nav-item text-nowrap">
      <a class="nav-link logout-link" href="LogoutServlet">ログアウト</a>
     </li>
    </ul>
   </nav>
    </header>

   <div class="container">
    <h1>ユーザ削除確認</h1>
    <form method="post" action="#" class="form-horizontal">
     <p>ログインID:${user.loginid}</p>
     <p>を本当に削除してもよろしいでしょうか。</p>




      <div class="text-center">

        <a href="UserListServlet">キャンセル</a>
        <button type="submit" name="act" value="ok">OK</button>
      </div>
    </form>
   </div>
</body>
</html>