<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ログイン画面</title>
<link href="css/original/login.css" rel="stylesheet">
</head>
<body>
   <form class="form-signin" action="LoginServlet" method="post">
    <div class="text-center">
     <h1>ログイン画面</h1>
     <p><span style="color:#ff0000;">${requestScope.errMsg}</span></p>
    </div>

    <div class="inputloginid">
     <label for="loginid">ログインID</label>
     <input type="text" name="loginid">
    </div>

    <br>

    <div class="inputpassword">
     <label for="password">パスワード</label>
     <input type="password" name="password">
    </div>

    <br>

    <div class="button_wrapper">
     <button class=btn type=submit>ログイン</button>
    </div>

  </form>
</body>
</html>