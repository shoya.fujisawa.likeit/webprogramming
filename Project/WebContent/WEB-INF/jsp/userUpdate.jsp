<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>ユーザ更新画面</title>
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/original/userUpdate.css" rel="stylesheet">
</head>
<body>
  <header>
      <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">



    <ul class="navbar-nav px-3">
     <li class="navbar-text">${userInfo.name} さん</li>
     <li class="nav-item text-nowrap">
      <a class="nav-link" href="LogoutServlet">ログアウト</a>
     </li>
    </ul>
   </nav>
    </header>

    <div class="container">
      <h1>ユーザ情報更新</h1>
      <p><span style="color:#ff0000;">${requestScope.errMsg}</span></p>
    <form method="post" action="#" class="form-horizontal">
      <div class="form-group row">
        <label for="loginId" class="col-sm-2 col-form-label">ログインID</label>
        <div class="col-sm-10">
         <input readonly type="text" name="loginid" id="loginid" value="${user.loginid}">
        </div>
      </div>

      <div class="form-group row">
        <label for="password" class="col-sm-2 col-form-label">パスワード</label>
        <div class="col-sm-10">
          <input type="password" class="form-control" name="password" id="password">
        </div>
      </div>

      <div class="form-group row">
        <label for="passwordConf" class="col-sm-2 col-form-label">パスワード(確認)</label>
        <div class="col-sm-10">
          <input type="password" class="form-control" name="passwordConf" id="passwordConf">
        </div>
      </div>

      <div class="form-group row">
        <label for="userName" class="col-sm-2 col-form-label">ユーザ名</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="userName" id="userName" value="${user.name}" >
        </div>
      </div>

      <div class="form-group row">
        <label for="birthDate" class="col-sm-2 col-form-label">生年月日</label>
        <div class="col-sm-10">
          <input type="date" class="form-control" name="birthDate" id="birthDate" value="${user.birthdate}">
        </div>
      </div>

      <div class="text-center">
        <button type="submit" value="検索">更新</button>
      </div>

      <div class="col-xs-4">
        <a href="UserListServlet">戻る</a>
      </div>

    </form>
    </div>
</body>
</html>