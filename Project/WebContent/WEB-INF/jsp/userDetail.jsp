<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <title>ユーザ詳細画面</title>
   <link href="css/bootstrap.min.css" rel="stylesheet">
   <link href="css/original/userDetail.css" rel="stylesheet">

</head>


<body>
<header>
      <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">



    <ul class="navbar-nav px-3">
     <li class="navbar-text">${userInfo.name} さん</li>
     <li class="nav-item text-nowrap">
      <a class="nav-link" href="LogoutServlet">ログアウト</a>
     </li>
    </ul>
   </nav>
    </header>

   <div class="container">
    <h1>ユーザ情報詳細参照</h1>

    <div class="userDetail">
      <div class="form-group row">
        <label for="loginId" class="col-sm-2 col-form-label">ログインID</label>
        <div class="col-sm-10">
          <p class="form-control-plaintext">${user.loginid}</p>
        </div>
      </div>

      <div class="form-group row">
        <label for="userName" class="col-sm-2 col-form-label">ユーザ名</label>
        <div class="col-sm-10">
          <p class="form-control-plaintext">${user.name}</p>
        </div>
      </div>

      <div class="form-group row">
        <label for="birthDate" class="col-sm-2 col-form-label">生年月日</label>
        <div class="col-sm-10">
          <p class="form-control-plaintext">${user.birthdate}</p>
        </div>
      </div>

      <div class="form-group row">
        <label for="createDate" class="col-sm-2 col-form-label">登録日時</label>
        <div class="col-sm-10">
          <p class="form-control-plaintext">${user.createdate}</p>
        </div>
      </div>

      <div class="form-group row">
        <label for="updateDate" class="col-sm-2 col-form-label">更新日時</label>
        <div class="col-sm-10">
          <p class="form-control-plaintext">${user.updatedate}</p>
        </div>
      </div>


      <div class="col-xs-4">
        <a href="UserListServlet">戻る</a>
      </div>

    </div>

   </div>
</body>
</html>