package model;

import java.io.Serializable;

public class User implements Serializable {
	private int id;
	private String loginid;
	private String name;
	private String birthdate;
	private String password;
	private String createdate;
	private String updatedate;

	public User() {
	}
	public User(String loginid,String name) {
		this.loginid = loginid;
		this.name = name;
	}

	public User(int id, String loginid, String name, String birthdate, String password, String createdate,
			String updatedate) {
		this.id = id;
		this.loginid = loginid;
		this.name = name;
		this.birthdate = birthdate;
		this.password = password;
		this.createdate = createdate;
		this.updatedate = updatedate;
	}

	public User(String loginid,String name,String birthdate) {
		this.loginid=loginid;
		this.name=name;
		this.birthdate=birthdate;
	}

	public User(String loginid,String name,String birthdate,String createdate,String updatedate) {
		this.loginid=loginid;
		this.name=name;
		this.birthdate=birthdate;
		this.createdate=createdate;
		this.updatedate=updatedate;
	}



	public int getid() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLoginid() {
		return loginid;
	}

	public void setLoginid(String loginid) {
		this.loginid = loginid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCreatedate() {
		return createdate;
	}

	public void setCreatedate(String createdate) {
		this.createdate = createdate;
	}

	public String getUpdatedate() {
		return updatedate;
	}

	public void setUpdatedate(String updatedate) {
		this.updatedate = updatedate;
	}


}
