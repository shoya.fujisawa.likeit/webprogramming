package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;


/**
 * Servlet implementation class UserAddServlet
 */
@WebServlet("/UserAddServlet")
public class UserAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserAddServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
		dispatcher.forward(request, response);

		HttpSession session = request.getSession();
		if(session.getAttribute("userInfo")==null) {
			response.sendRedirect("LogoutServlet");
		}

	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		String loginid=request.getParameter("user-id");
		String password=request.getParameter("password");
		String password2=request.getParameter("password-confirm");
		String name=request.getParameter("user-name");
		String birthdate=request.getParameter("birth-day");

		UserDao userDao=new UserDao();
		User user=userDao.usersearch(loginid);

		if(user!=null) {
			request.setAttribute("userId", loginid);
			request.setAttribute("userName", name);
			request.setAttribute("userBirthdate", birthdate);
			request.setAttribute("errMsg", "入力された内容は正しくありません");



			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
			dispatcher.forward(request, response);






			return;
		}

		if(!(password.equals(password2))) {


			request.setAttribute("userId", loginid);
			request.setAttribute("userName", name);
			request.setAttribute("userBirthdate", birthdate);
			request.setAttribute("errMsg", "入力された内容は正しくありません");



			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
			dispatcher.forward(request, response);






			return;



		}

		if(loginid.equals("")||password.equals("")||password2.equals("")||name.equals("")||birthdate.equals("")) {

			request.setAttribute("userId", loginid);
			request.setAttribute("userName", name);
			request.setAttribute("userBirthdate", birthdate);
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
			dispatcher.forward(request, response);

			return;
		}






		userDao.addInfo(loginid, password, password2, name, birthdate);


		response.sendRedirect("UserListServlet");






	}

}
