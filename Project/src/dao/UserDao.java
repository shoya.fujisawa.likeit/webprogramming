package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;


public class UserDao {
	public User findByLoginInfo(String loginid, String password) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String source=password;
    		Charset charset = StandardCharsets.UTF_8;
    		String algorithm = "MD5";
    		byte[] bytes;

		    bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
			String result = DatatypeConverter.printHexBinary(bytes);

			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginid);
            pStmt.setString(2, result);
            ResultSet rs = pStmt.executeQuery();

            if (!rs.next()) {
                return null;
            }

            String loginIdData = rs.getString("login_id");
            String nameData = rs.getString("name");
            return new User(loginIdData, nameData);

		}catch (SQLException | NoSuchAlgorithmException   e) {
			e.printStackTrace();
			return null;
		}finally {
			if (conn != null) {
	          	try {
	                		conn.close();
	            	} catch (SQLException e) {
	                		e.printStackTrace();
	                		return null;
	            	}
		         }
	          }


         }

	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id<>'admin'";

			Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
            	int id = rs.getInt("id");
                String loginid = rs.getString("login_id");
                String name = rs.getString("name");
                String birthdate = rs.getString("birth_date");
                String password = rs.getString("password");
                String createdate = rs.getString("create_date");
                String updatedate = rs.getString("update_date");
                User user = new User(id, loginid, name, birthdate, password, createdate, updatedate);

                userList.add(user);
            }
		}catch(SQLException e){
			e.printStackTrace();
            return null;
		}finally {
			 if (conn != null) {
	                try {
	                    conn.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                    return null;
	                }
	            }
		}
		return userList;
	}

	public User usersearch(String loginid) {
		Connection conn=null;
		try {
			conn=DBManager.getConnection();

			String sql="select*from user where login_id=?";

			PreparedStatement stmt=conn.prepareStatement(sql);
			stmt.setString(1, loginid);
			ResultSet rs = stmt.executeQuery();
			if (!rs.next()) {
                return null;
            }
			String loginId=rs.getString("login_id");
			return new User();
		}catch (SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if (conn != null) {
	          	try {
	                		conn.close();
	            	} catch (SQLException e) {
	                		e.printStackTrace();
	                		return null;
	            	}
		         }
	          }
	}


    public void addInfo(String loginid,String password,String password2,String name,String birthdate){
    	Connection conn=null;

    	try {
    		conn=DBManager.getConnection();
    		String source=password;
    		Charset charset = StandardCharsets.UTF_8;
    		String algorithm = "MD5";
    		byte[] bytes;

		    bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
			String rs = DatatypeConverter.printHexBinary(bytes);





    		String sql="insert into user(login_id,name,birth_date,password,create_date,update_date) values(?,?,?,?,now(),now())";




    		PreparedStatement stmt=conn.prepareStatement(sql);
    		stmt.setString(1, loginid);
    		stmt.setString(2, name);
    		stmt.setString(3, birthdate);
    		stmt.setString(4, rs);


    		int result=stmt.executeUpdate();




    	}catch(SQLException | NoSuchAlgorithmException e){
			e.printStackTrace();


    	}finally {
    		if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
    	}
    }

    public List<User> searchUser(String loginid,String name,String birthdatestart,String birthdateend) {
    	Connection conn=null;
    	List<User> userList = new ArrayList<User>();


    	try {
    		conn = DBManager.getConnection();


			String sql = "SELECT * FROM user WHERE login_id<>'admin'";

			if(!loginid.equals("")) {
				sql += " AND login_id = '" + loginid + "'";
			}

			if(!name.equals("")) {
				sql += " AND name LIKE '%" + name + "%'";
			}

			if(!birthdatestart.equals("")) {
				sql += " AND birth_date>'" + birthdatestart + "'";
			}

			if(!birthdateend.equals("")) {
				sql += " AND birth_date<'" + birthdateend + "'";
			}




			Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
            	int id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                String userName = rs.getString("name");
                String birthdate = rs.getString("birth_date");
                String password = rs.getString("password");
                String createdate = rs.getString("create_date");
                String updatedate = rs.getString("update_date");
                User user = new User(id, loginId, userName, birthdate, password, createdate, updatedate);

                userList.add(user);
            }





    		/*String sql="SELECT * FROM user WHERE login_id = ? and name LIKE ? and birth_date BETWEEN ? AND ?";

    		PreparedStatement pStmt = conn.prepareStatement(sql);


    		pStmt.setString(1, loginid);
    		pStmt.setString(2, "%"+name+"%");
    		pStmt.setString(3, birthdate);
    		pStmt.setString(4, birthdate);
    		*/


    		//ResultSet rs = pStmt.executeQuery();

    		/*if (!rs.next()) {
                return null;
            }
    		String loginIdData = rs.getString("login_id");
            String nameData = rs.getString("name");
            String birthDate = rs.getString("birth_date");



            return new User(loginIdData,nameData,birthDate);*/

    	}catch (SQLException e) {
    		e.printStackTrace();
    		return null;
    	}finally {
			if (conn != null) {
	          	try {
	                		conn.close();
	            	} catch (SQLException e) {
	                		e.printStackTrace();
	                		return null;
	            	}
		         }
	          }
    	return userList;
    }

    public User userdetail(int id) {
    	Connection conn = null;

    	try {
    		conn = DBManager.getConnection();

    		String sql = "SELECT * FROM user WHERE id=?";

    		PreparedStatement stmt = conn.prepareStatement(sql);
    		stmt.setInt(1, id);

            ResultSet rs = stmt.executeQuery();

            if(!rs.next()) {
            	return null;
            }

            String loginidData=rs.getString("login_id");
            String nameData=rs.getString("name");
            String birthdateData=rs.getString("birth_date");
            String createdateData=rs.getString("create_date");
            String updatedateData=rs.getString("update_date");




            return new User(loginidData,nameData,birthdateData,createdateData,updatedateData);


    	}catch(SQLException e){
    		e.printStackTrace();
    		return null;
    	}finally {
    		if (conn != null) {
	          	try {
	                		conn.close();
	            	} catch (SQLException e) {
	                		e.printStackTrace();
	                		return null;
	            	}
		         }
    	}


    }

    public void userupdate(int id,String password,String password2,String name,String birthdate){
    	Connection conn = null;
    	try {
    		conn = DBManager.getConnection();

    		String source=password;
    		Charset charset = StandardCharsets.UTF_8;
    		String algorithm = "MD5";
    		byte[] bytes;

		    bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
			String rs = DatatypeConverter.printHexBinary(bytes);

    		String sql="UPDATE user SET password=?,name=?,birth_date=?,update_date=now() WHERE id=?";

    		PreparedStatement stmt = conn.prepareStatement(sql);
    		stmt.setString(1,rs);
    		stmt.setString(2, name);
    		stmt.setString(3, birthdate);
    		stmt.setInt(4, id);

    		int result=stmt.executeUpdate();
    	}catch(SQLException | NoSuchAlgorithmException e) {
    		e.printStackTrace();
    	}finally {
    		if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
    	}
    }

    public void userupdate2(int id,String password,String password2,String name,String birthdate) {
    	Connection conn = null;
    	try {
    		conn = DBManager.getConnection();

    		String sql="UPDATE user SET name=?,birth_date=? WHERE id=?";
    		PreparedStatement stmt = conn.prepareStatement(sql);
    		stmt.setString(1, name);
    		stmt.setString(2, birthdate);
    		stmt.setInt(3, id);

    		int result=stmt.executeUpdate();
    	}catch(SQLException e) {
    		e.printStackTrace();
    	}finally {
    		if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
    	}
    }

    public void userdelete(int id) {
    	Connection conn = null;
    	try {
    		conn = DBManager.getConnection();
    		String sql="DELETE FROM user WHERE id=?";
    		PreparedStatement stmt = conn.prepareStatement(sql);
    		stmt.setInt(1, id);

    		int result=stmt.executeUpdate();
    	}catch(SQLException e) {
    		e.printStackTrace();
    	}finally {
    		if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
    	}
    }
    }






